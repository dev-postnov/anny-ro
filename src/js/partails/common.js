//Preloader

document.body.onload = function() {
  var preloader = document.getElementById('loader');

  if (!preloader.classList.contains('done')) {
    preloader.classList.add('done');
  }
}


$(window).on('load', function() {



    //slider social post

    $('.social-post').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 1800,
      swipe: true,
      swipeToSlide: true,
      infinite: true,
      responsive: [
          {
            breakpoint: 1250,
            settings: {
              slidesToShow: 7,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
    }).on("mousewheel", function (event) {
          event.preventDefault();
      if (event.deltaX > 0 || event.deltaY < 0) {
          $(this).slick("slickNext");
      } else if (event.deltaX < 0 || event.deltaY > 0) {
          $(this).slick("slickPrev");
      }
  });

})



$(document).ready(function() {

  $(document).on('click', '.js-nav-toggle', function() {
    $('.nav').addClass('show');
  })

  $('.nav__cross').click(function() {
    $(this).parent().removeClass('show');
  })

  $(document).on('click', '.show .nav__item', function(){
    $(this).closest('.nav').removeClass('show');
  })




  //ScrollTo
  $('.nav__item').click(function() {
    var elem = $(this).attr('data-element'),
        offset = $(elem).offset().top;

    $('html,body').animate({
      scrollTop: offset - 100
    },1200)

  })


  //Fixed Header
  $(window).scroll(function() {
    var offset = $(window).scrollTop();
    var offsetOfWorks = $('#works').offset().top - 500;
    var offsetOfLinks = $('#social-link').offset().top - 700;

    //header fixed

    //animate work

    if ($(window).scrollTop() > offsetOfWorks) {
        $('.works__item').not('.animated').each(function(i, elem) {
          var that = $(this);
            setTimeout(function() {
               that.removeClass('hidden').addClass('animated fadeInUp visible');
            }, i * 200)
        })
    }

    if ($(window).scrollTop() > offsetOfLinks) {
        $('.contact-animatation').not('.animated').each(function(i, elem) {
          var that = $(this);
            setTimeout(function() {
               that.removeClass('hidden').addClass('animated fadeInUp visible');
            }, i * 200)
        })
    }

  })


  //View Work
  $('.works__item').click(function() {
    var img  = $(this).find('.works__img').attr('src'),
        id = $(this).attr('data-post-id'),
        text = $(this).find('.works__text').clone();

    var dataFullImage = $(this).find('.works__img').attr('data-full-image');

    if (dataFullImage.indexOf('default') == -1){
      img = $(this).find('.works__img').attr('data-full-image');
    }

    $('.view-modal__img').attr('src', img);
    $('.view-modal__desc').html('').append(text);
    $('.view-modal').fadeIn(500).attr('data-post-id', id);

    $('body').css('overflow','hidden');
  })

  $('.view-modal__cross').click(function() {
    $(this).parent().fadeOut(500);
    $('body').css('overflow','auto');
  })






  // Switch work
  $('.view-modal__arrow-right,.view-modal__arrow-left').click(function(e) {
    //switchImage(e.target);
    switchWork(e.target);
  })





  $('.about-me__slider').slick({
    dots: true,
    infinite: true,
    speed: 1200,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 5000
  });





//Swipe

 //Enable swiping...
 $(".view-modal__arrow-left").swipe( {
   //Генерируем обработчик swipe для всех направлений
   swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
      if(direction == 'right') {
              console.log(this);
        switchWork(this)
      }
   },

   //threshold – расстояние сдвига контейнера, при котором срабатывает событие. По умолчанию — 75px, установите значение в 0 для теста!
   threshold: 30
 });

 //Enable swiping...
 $(".view-modal__arrow-right").swipe({
   //Генерируем обработчик swipe для всех направлений
   swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
    if(direction == 'left') {
      console.log(this);
       switchWork(this)
    }
   },

   //threshold – расстояние сдвига контейнера, при котором срабатывает событие. По умолчанию — 75px, установите значение в 0 для теста!
   threshold: 30
 });





/*===============================
          Functions
=================================*/


function switchWork(target){

  var id = $(target).closest('.view-modal').attr('data-post-id'),
  maxId = $('.works__item').length;

  if ($(target).hasClass('view-modal__arrow-left')){
    id--;
    if (id < 1) id = maxId;

    getAndSetDataSwitchAndId(id);
  }


  if ($(target).hasClass('view-modal__arrow-right')){
    id++;
    if (id > maxId) id = 1;

    getAndSetDataSwitchAndId(id);
  }

}





function getAndSetDataSwitchAndId(id){
  var work = $('.works__item[data-post-id="'+id+'"]');

  //получаем данные с другой работы
  var workText = work.find('.works__text').clone(),
      dataFullImage = work.find('.works__img').attr('data-full-image'),
      imageSrc;


  if (dataFullImage.indexOf('default') == -1 && dataFullImage.length != 0){
    imageSrc = work.find('.works__img').attr('data-full-image');
  }else{
     imageSrc = work.find('.works__img').attr('src');
  }


  //Ставим данные в окно
  $('.view-modal__img').attr('src', imageSrc);
  $('.view-modal__desc').html('').append(workText);

  //меняем id окна
  $('.view-modal').attr('data-post-id', id);

}









});//end ready
